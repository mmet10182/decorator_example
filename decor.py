def upper_case(func):
    def trtr(*args, **kwargs):
        return func(*args).upper()
    return trtr


def with_arg(a):
    def wrap(func):
        def wrap_wrap(*args, **kwargs):
            return func(*args) + a
        return wrap_wrap
    return wrap


@upper_case
@with_arg('123')
def test1(stroka):
    return stroka


if __name__ == '__main__':
    print(test1('dsf'))